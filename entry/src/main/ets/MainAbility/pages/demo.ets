/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { LiveEventBus } from '@ohos/liveeventbus'
import { Lifecycle } from '@ohos/liveeventbus'
import { State } from '@ohos/liveeventbus'
import router from '@system.router';
import prompt from '@system.prompt';


const showToast = function (str: string) {
  console.log('showToast:' + str)
  try {
    prompt.showToast({
      message: str,
      duration: 2000,
    });
  } catch (err) {
    console.log('showToast err:' + err)
  }

}


var observer = {
  onChanged(s) {
    showToast(s)
  }
};

function startService() {
  LiveEventBus.get<string>(KEY_TEST_BROADCAST)
    .observeForever(observer);
  LiveEventBus
    .get<string>(KEY_TEST_BROADCAST_IN_APP)
    .observeForever(observer);
  LiveEventBus
    .get<string>(KEY_TEST_BROADCAST_GLOBAL)
    .observeForever(observer);
}

function destroyService() {
  LiveEventBus
    .get<string>(KEY_TEST_BROADCAST)
    .removeObserver(observer);
  LiveEventBus
    .get<string>(KEY_TEST_BROADCAST_IN_APP)
    .removeObserver(observer);
  LiveEventBus
    .get<string>(KEY_TEST_BROADCAST_GLOBAL)
    .removeObserver(observer);
}

const KEY_TEST_OBSERVE = "key_test_observe";
const KEY_TEST_OBSERVE_FOREVER = "key_test_observe_forever";
const KEY_TEST_STICKY = "key_test_sticky";
const KEY_TEST_MULTI_THREAD_POST = "key_test_multi_thread_post";
const KEY_TEST_MSG_SET_BEFORE_ON_CREATE = "key_test_msg_set_before_on_create";
const KEY_TEST_CLOSE_ALL_PAGE = "key_test_close_all_page";
const KEY_TEST_ACTIVE_LEVEL = "key_test_active_level";
const KEY_TEST_ACTIVE_LEVEL_SINGLE = "key_test_active_level_single";
const KEY_TEST_BROADCAST = "key_test_broadcast";
const KEY_TEST_BROADCAST_IN_APP = "key_test_broadcast_in_app";
const KEY_TEST_BROADCAST_GLOBAL = "key_test_broadcast_global";
const KEY_TEST_DELAY_LIFE = "key_test_delay_life";

let that

@Entry
@Component
struct Demo {
  private mLifecycle: Lifecycle;
  scroller: Scroller= new Scroller()
  sendCount: number = 0;
  receiveCount: number = 0;
  randomKey: string = null;
  observer = {
    onChanged(s) {
      showToast(s);
    }
  };

  aboutToAppear() {
    that = this
    this.mLifecycle = new Lifecycle(State.STARTED)

    LiveEventBus
      .get<string>(KEY_TEST_OBSERVE)
      .observe(this, {
        onChanged(s) {
          showToast(s);
        }
      });
    LiveEventBus
      .get<string>(KEY_TEST_OBSERVE_FOREVER)
      .observeForever(this.observer);
    LiveEventBus
      .get<boolean>(KEY_TEST_CLOSE_ALL_PAGE)
      .observe(this, {
        onChanged(b: boolean) {
          if (b) {
            console.log('action ---> close all ')
            router.clear()
            router.back()
          }
        }
      });
    LiveEventBus
      .get<string>(KEY_TEST_MULTI_THREAD_POST)
      .observe(this, {
        onChanged(s) {
          that.receiveCount++;
        }
      });
    LiveEventBus
      .get<string>(KEY_TEST_ACTIVE_LEVEL)
      .observe(this, {
        onChanged(s) {
          showToast("Receive message: " + s);
        }
      });
    LiveEventBus
      .get<string>(KEY_TEST_ACTIVE_LEVEL_SINGLE)
      .observe(this, {
        onChanged(s) {
          showToast("Receive message: " + s);
        }
      });
    LiveEventBus
      .get<string>(KEY_TEST_DELAY_LIFE)
      .observe(this, {
        onChanged(s) {
          showToast("Receive message: " + s);
        }
      });
    LiveEventBus.get<string>('DemoEvent')
      .observe(this, {
        onChanged(demoEvent) {
          console.log('DemoEvent:' + JSON.stringify(demoEvent))
          let event = JSON.parse(demoEvent)
          showToast("Receive message: " + event.content);
        }
      });

    startService()

  }

  aboutToDisappear() {
    destroyService()
    this.mLifecycle.markState(State.DESTROYED)
  }

  getLifecycle(): Lifecycle{
    return this.mLifecycle
  }

  build() {
    Column({ space: 10 }) {
      Text('LiveEventBus Demo')
        .fontSize(30)
        .fontWeight(FontWeight.Bold)
      Scroll() {
        Column({ space: 5 }) {
          Button('发送消息').onClick(event => {
            this.sendMsgByPostValue()
          })
          Button('发送消息给observeForever()注册的订阅者').onClick(event => {
            this.sendMsgToForeverObserver()
          })
          Button('Start一个新的Ability（测试close all）').onClick(event => {
            this.startNewAbility()
          })
          Button('关闭All Ability').onClick(event => {
            this.closeAll()
          })
          Button('发送消息给Sticky Receiver').onClick(event => {
            this.sendMsgToStickyReceiver()
          })
          Button('Start Sticky Ability（测试Sticky）').onClick(event => {
            this.startStickyAbility()
          })
          Button('测试PostValue会丢失消息的问题').onClick(event => {
            this.postValueCountTest()
          })
          Button('测试动态注册一个Observer会收到之前发送的消息的问题').onClick(event => {
            this.testMessageSetBefore()
          })
          Button('发送的消息给之前动态注册的Observer').onClick(event => {
            this.sendMessageSetBefore()
          })
          Button('测试Observer active level').onClick(event => {
            this.testObserverAbilityLevel()
          })
          Button('测试跨进程发送消息').onClick(event => {
            this.testBroadcast()
          })
          Button('测试延迟发送带生命周期').onClick(event => {
            this.testDelayLife()
          })
          Button('发送App内广播').onClick(event => {
            this.testBroadcastInApp()
          })
          Button('发送全局广播').onClick(event => {
            this.testBroadcastGlobal()
          })
          Button('发送DemoEvent').onClick(event => {
            this.sendDemoEvent()
          })
        }.width('100%')
        .margin({ bottom: 150 })

      }
      .scrollable(ScrollDirection.Vertical).scrollBar(BarState.On)
      .scrollBarColor(Color.Gray).scrollBarWidth(30)
    }.gridSpan(10)

  }

  sendMsgByPostValue() {
    console.log('click sendMsgByPostValue')

    LiveEventBus.get(KEY_TEST_OBSERVE)
      .post("Message By PostValue: " + nextInt(100));
  }

  sendMsgToForeverObserver() {
    LiveEventBus.get(KEY_TEST_OBSERVE_FOREVER)
      .post("Message To ForeverObserver: " + nextInt(100));
  }

  startNewAbility() {
    router.push({
      uri: 'pages/demo',
      params: {},
    });
  }

  closeAll() {
    LiveEventBus.get(KEY_TEST_CLOSE_ALL_PAGE).post(true);
  }

  sendMsgToStickyReceiver() {
    LiveEventBus.get(KEY_TEST_STICKY)
      .post("Message Sticky: " + nextInt(100));
  }

  startStickyAbility() {
    router.push({ uri: 'pages/Sticky' });
  }

  postValueCountTest() {
    this.sendCount = 1000;
    this.receiveCount = 0;
    for (let i = 0; i < this.sendCount; i++) {
      LiveEventBus.get(KEY_TEST_MULTI_THREAD_POST).post("test_data");
    }

    setTimeout(() => {
      showToast("sendCount: " + this.sendCount + " | receiveCount: " + this.receiveCount)
    }, 1000)
  }

  testMessageSetBefore() {
    //先动态生成一个key
    this.randomKey = "key_random_" + nextInt(100);
    //然后发出一个消息
    LiveEventBus.get<string>(this.randomKey).post("msg set before");
    //然后订阅这个消息
    LiveEventBus
      .get<string>(this.randomKey)
      .observe(this, {
        onChanged(s) {
          showToast(s)
        }
      });
  }

  sendMessageSetBefore() {
    LiveEventBus.get<string>(this.randomKey).post("msg set after");
  }

  testObserverAbilityLevel() {
    router.push({ uri: 'pages/ObserverAbilityLevel' });
  }

  testBroadcast() {
    LiveEventBus
      .get(KEY_TEST_BROADCAST)
      .postAcrossApp("broadcast msg");
  }

  testDelayLife() {
    router.push({
      uri: 'pages/PostDelay',
      params: {},
    });
  }

  testBroadcastInApp() {
    LiveEventBus
      .get(KEY_TEST_BROADCAST_IN_APP)
      .postAcrossProcess("broadcast msg");
  }

  testBroadcastGlobal() {
    LiveEventBus
      .get(KEY_TEST_BROADCAST_GLOBAL)
      .postAcrossApp("broadcast msg");
  }

  sendDemoEvent() {
    LiveEventBus
      .get<string>('DemoEvent')
      .post(JSON.stringify(new DemoEvent("Hello world")));
  }
}

class DemoEvent {
  content: string

  constructor(content: string) {
    this.content = content
  }
}

const nextInt = function (num: number) {
  return getRandomNumInt(0, num)
}
const getRandomNumInt = function (min: number, max: number) {
  var Range = max - min;
  var Rand = Math.random(); //获取[0-1）的随机数
  return (min + Math.round(Rand * Range)); //放大取整
}